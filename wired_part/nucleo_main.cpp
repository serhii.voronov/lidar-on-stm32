#include "mbed.h"

//MOTOR:
PwmOut PWMA(D6);
DigitalOut AIN1(D4);
DigitalOut AIN2(D5);
DigitalOut STBY(D3);

//HALL SENSOR
DigitalIn hall_sensor(PC_1);

//COIL:
PwmOut coil(PA_8);

//SERIAL:
Serial input(A0, A1, 9600); // tx, rx
Serial pc(USBTX, USBRX, 115200);

int main(){
    pc.printf("Started!\n");

    //DO NOT CHANGE!!!!
    coil.period(0.000015384615f);      // 1/65000, 65 kHz
    coil.write(0.50f);      // 50% duty cycle, relative to period
    //DO NOT CHANGE!!!!

    STBY=1;
    //STBY=0;
    PWMA.period(0.00001111f);// 90khz      up to 100kHz
    PWMA.write(0.10f);
    AIN1=0;
    AIN1=1;

    int soft_start = 2;
    int rot_time = 0;
    int rot_time_tmp = 0;

    // storage for all sensors:
    uint8_t sensor_1_array[200],
            sensor_2_array[200],
            sensor_3_array[200];

    // in which position is our sensor (0-120 or 120-240 or 240-360)
    uint8_t data_state = 0;

    // in fact 1 byte start(0x01) packet, 3 bytes - 3 measurments, control summ, final byte(0x00):
    uint8_t recieved_package[7];

    int len = 0;
    int data_len = 0;
    bool hall_sensor_redy = 1;

    while(1) {
        if(input.readable()) {
            uint8_t byte_got = input.getc();

            //if it's start bit:
            if(byte_got == 1){
                len = 0;
            }

            if(len < 6){
                recieved_package[len++] = byte_got;

                if(len == 6 && byte_got == 0){
                    rot_time_tmp++;
                    if(!soft_start){
                        //protection from the lidar stuck, from the overflow
                        if(data_len < 200){
                            sensor_1_array[data_len] = recieved_package[1];
                            sensor_2_array[data_len] = recieved_package[2];
                            sensor_3_array[data_len++] = recieved_package[3];
                        }
                        //data_state - lidar position
                        if((data_state == 0 && rot_time_tmp > rot_time/3) ||  // if we measured 0 to 120 degrees
                           (data_state == 1 && rot_time_tmp > (rot_time*2)/3) ){ // if we measured 120 to 240
                            pc.printf("%i", data_len*3);

                            //depend on the sensor position we send the data in the different order
                            if(data_state == 0){
                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_1_array[i]);

                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_3_array[i]);

                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_2_array[i]);

                            }
                            if(data_state == 1){
                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_2_array[i]);

                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_1_array[i]);

                                for(int i = 0; i < data_len; i++)
                                    pc.printf(" %i", sensor_3_array[i]);
                            }

                            pc.printf("\n");

                            data_state++;
                            data_len = 0;
                        }
                    }
                }
            }
        }
        //if we in measurment from 240 to 360, here it is up to the hall sensor because of keeping sinchronization in well status
        if (hall_sensor==0){
            if(hall_sensor_redy){
                hall_sensor_redy = 0;

                rot_time = rot_time_tmp;
                rot_time_tmp = 0;

                //we ignore measures before the first circle move
                if(soft_start)
                    soft_start--;

                pc.printf("%i", data_len*3);

                for(int i = 0; i < data_len; i++)
                    pc.printf(" %i", sensor_3_array[i]);

                for(int i = 0; i < data_len; i++)
                    pc.printf(" %i", sensor_2_array[i]);

                for(int i = 0; i < data_len; i++)
                    pc.printf(" %i", sensor_1_array[i]);

                pc.printf("\n");

                data_state = 0;
                data_len = 0;
            }
        }
        else{
            hall_sensor_redy = 1;
        }
    }
}
