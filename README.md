# LIDAR on STM32

<h1 align="center">Welcome to my LIDAR project 👋</h1>

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/TThuqPODTBY/0.jpg)](https://www.youtube.com/watch?v=TThuqPODTBY)

## ✨ Description

The task of this project was to create a `wireless lidar` based on `STM32`




## 🚀 Project architecture 

![Alt text](images/diagram.png?raw=true "Title")

## ⚡  Schematic 

<p align="center">
  <img src="images/transmitter.png?raw=true" width="700" title="hover text">
</p>
<p align="center">
  <img src="images/reciever.png?raw=true" width="700" title="hover text">
</p>
<p align="center">
  <img src="images/scheme2.png?raw=true" width="700" title="hover text">
</p>

## 🌡️  Temperature tests 

<p align="center">
  <img src="images/temp1.png?raw=true" height="300" title="hover text">
  <img src="images/temp2.jpg?raw=true" height="300" alt="accessibility text">
</p>

## 👤  Author

 **Voronov Serhii**

- LinkedIn: [@serhii-voronov](https://www.linkedin.com/in/serhii-voronov/)
- Github: [@serhii.voronov](https://gitlab.com/serhii.voronov)

## 🫶 Show your support

Please ⭐️ this repository if this project helped you!

## 📝 License

Copyright © 2022 [Serhii Voronov](https://gitlab.com/serhii.voronov).<br />


---
