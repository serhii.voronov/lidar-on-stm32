#include "mbed.h"
#include "VL53L1X.h"
 
Serial pc(PA_2, PA_3, 9600);   

//Start i2c link for laser-sensors
I2C i2c(PF_0, PF_1);  

//3 timers for laser-sensors
Timer timer1;
Timer timer2;
Timer timer3;

//3 laser sensors
VL53L1X sensor1(&i2c, &timer1);
VL53L1X sensor2(&i2c, &timer2);
VL53L1X sensor3(&i2c, &timer3);

// 3 pins to control on/off of the each laser-sensor
DigitalOut xshut_1(PB_1);
DigitalOut xshut_2(PA_7);
DigitalOut xshut_3(PA_6);

int main() {
  wait(1.0);
  //turn on only first sensor:
  xshut_1 =1;
  xshut_2 =0;
  xshut_3 =0;
  wait(1.0);
  
  //********** FIRST SENSOR INITIALIZATION: **********
  sensor1.setTimeout(500);
  pc.printf("Init 1 sensor!\r\n");
  if (!sensor1.init()){
    pc.printf("Failed to detect and initialize sensor!"); while (1){wait(1.0);}}
  sensor1.setAddress(42);
  sensor1.setDistanceMode(VL53L1X::Medium); 
  //long distance allow up to 50 ms measurement, 33 - medium, 20 short
  sensor1.setMeasurementTimingBudget(33000);
  sensor1.startContinuous(33);
  pc.printf("NEW 1 sensor address: %u\r\n", sensor1.getAddress() );
  
  
  
  //*****
  wait(1);
  xshut_2 =1;
  wait(1);
  
  
   //********** SECOND SENSOR INITIALIZATION: **********
  sensor2.setTimeout(500);
  pc.printf("Init 2 sensor!\r\n");
  if (!sensor2.init()){
    pc.printf("2Failed to detect and initialize sensor!"); while (1){wait(1.0);}}
  sensor2.setAddress(43);
  sensor2.setDistanceMode(VL53L1X::Medium);
  //long distance allow up to 50 ms measurement, 33 - medium, 20 short
  sensor2.setMeasurementTimingBudget(33000);
  sensor2.startContinuous(33);
  pc.printf("NEW 2 sensor address: %u\r\n", sensor2.getAddress() );
 
  //*****
  wait(1);
  xshut_3 =1;
  wait(1);
  
  
  //********** THIRD SENSOR INITIALIZATION: **********
  sensor3.setTimeout(500);
  pc.printf("Init 3 sensor!\r\n");
  if (!sensor3.init()){
    pc.printf("3 Failed to detect and initialize sensor!"); while (1){wait(1.0);}}
  sensor3.setAddress(44);
  sensor3.setDistanceMode(VL53L1X::Medium);
  //long distance allow up to 50 ms measurement, 33 - medium, 20 short
  sensor3.setMeasurementTimingBudget(33000);
  sensor3.startContinuous(33);
  pc.printf("NEW 3 sensor address: %u\r\n", sensor3.getAddress());
  
  //********** ALL SENSORS ON: **********
  wait(1.0);
  xshut_1 =1;
  xshut_2 =1;
  xshut_3 =1;
  wait(1.0);
  

  while (1){
    //read all 3 sensors and get data in 'mm' 
    int s1 = sensor1.read();
    int s2 = sensor2.read();
    int s3 = sensor3.read();
    
    //noise cancellation:
    if(s1 > 2550)
        s1 = 2550;
    if(s1 < 20)
        s1 = 20;
    
    if(s2 > 2550)
        s2 = 2550;
    if(s2 < 20)
        s2 = 20;
    
    if(s3 > 2550)
        s3 = 2550;
    if(s3 < 20)
        s3 = 20;
        
    //convert to 'cm' to fit the number to the one byte
    uint8_t s1_byte = s1 / 10;
    uint8_t s2_byte = s2 / 10;
    uint8_t s3_byte = s3 / 10;
    
    //send data in format:
    //1 byte - start bit
    //3 byte - three measurments
    //1 byte - control summ
    //1 byte - stop byte
    pc.putc(0x01);
    pc.putc(s1_byte);
    pc.putc(s2_byte);
    pc.putc(s3_byte);
    pc.putc((uint8_t)(s1_byte+s2_byte+s3_byte));
    pc.putc(0x00);

    //if sensor is dead/unconnected - timeout
    if (sensor1.timeoutOccurred()){
      pc.printf("1 TIMEOUT\r\n");
    }
    if (sensor2.timeoutOccurred()){
      pc.printf("2 TIMEOUT\r\n");
    }
    if (sensor3.timeoutOccurred()){
      pc.printf("3 TIMEOUT\r\n");
    }
  }
}