import numpy as np
import matplotlib.pyplot as plt
import serial
import math

BOARD_SIZE = 100

'''
Function made for parsing data from the sensor, so in fact 
function just split the line and send data from it. 
'''
def parse_serial(inp):
    line = (inp.decode("utf-8"))[:-1].split(" ")
    amount_measures = int(line[0])
    measures = []
    for i in range(1, amount_measures + 1):
        measures.append(line[i])
    # amount_measures - how many measures we gonna have in next symbols
    return amount_measures, measures


print("start")
ser = serial.Serial()
ser.baudrate = 115200

# TODO! DEFINE PORT!
ser.port = '/dev/tty.usbmodem143203'

while 1:
    try:
        print("opening serial...", ser.open())
        break
    except:
        pass
print("serial status:", ser)

#Preparing our graph space:
fig = plt.figure(figsize=(BOARD_SIZE, BOARD_SIZE))
x = np.linspace(-BOARD_SIZE, BOARD_SIZE, 1000)
y = np.linspace(-BOARD_SIZE, BOARD_SIZE, 1000)

a, b = np.meshgrid(x, y)
C = a ** 2 + b ** 2 - 0.2
figure, axes = plt.subplots()
axes.contour(a, b, C, [0])
axes.set_aspect(1)
plt.show()

while 1:
    print("==============================================")
    try:
        serial_get = ser.readline()
    except:
        print("SERIAL DEAD, EXIT....")
        exit()

    # skip noise:
    if len(serial_get) < 6:
        continue
    print("Got from serial:", serial_get)

    # after receiving the line we should parse it:
    amount_measures, measures = parse_serial(serial_get)

    # how many degree gonna be on on measure:
    degree_step = 360 / amount_measures

    points_to_dispay_x = []
    points_to_dispay_y = []

    # listing all points:
    for i in range(amount_measures):
        degree = i * degree_step
        distance = measures[i]
        print("Degree:", degree, "; Distance:", distance)
        # count X distance:
        points_to_dispay_x.append(math.cos(math.radians(degree)) * float(distance))
        # count Y distance:
        points_to_dispay_y.append(math.sin(math.radians(degree)) * float(distance))
    # Show all measured points
    plt.scatter(points_to_dispay_x, points_to_dispay_y)
    # Circle in the center of measuring, so in fact LIDAR position
    plt.scatter(0, 0, s=100)
    # Default plot size:
    plt.xticks([-50, 50])
    plt.yticks([-50, 50])
    plt.show()


